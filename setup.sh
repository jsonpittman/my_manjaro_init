sudo pacman -S feh alacritty fish i3-wm i3status dmenu vscode brave mame amfora virtualbox pcmanfm atom qutebrowser nextcloud vim 
sudo pip3 install protonvpn-cli
curl -sLf https://spacevim.org/install.sh | bash
chsh -s /usr/bin/fish
mkdir ~/Documents/Wallpapers
cp wallpapers/* ~/Documents/Wallpapers/
feh --randomize --bg-fill ~/Documents/Wallpapers/*
cp -r config/* ~/.config/
mkdir ~/.mame
mkdir ~/.mame/roms
cp coco3.zip ~/.mame/roms/

